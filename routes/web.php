<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', 'SiteController@Index')->name('/');
Route::get('/books/{user_id}', 'SiteController@Books')->name('/books/{user_id}');
Route::get('/all-books', 'SiteController@AllBooks')->name('/all-books');
Route::get('/authors', 'SiteController@Authors')->name('/authors');
Route::get('/users', 'SiteController@Users')->name('/users');

Route::get('/book/add', 'BookController@Add')->name('/book/add');
Route::get('/book/view/{id}', 'BookController@View')->name('/book/view/{id}');
Route::get('/book/edit/{id}', 'BookController@Edit')->name('/book/edit/{id}');
Route::get('/book/delete/{id}', 'BookController@Delete')->name('/book/delete/{id}');
Route::post('/book/create', 'BookController@Create')->name('/book/create');
Route::post('/book/update/{id}', 'BookController@Update')->name('/book/update/{id}');

Route::get('/user/add', 'UserController@Add')->name('/user/add');
Route::get('/user/edit/{id}', 'UserController@Edit')->name('/user/edit/{id}');
Route::get('/user/delete/{id}', 'UserController@Delete')->name('/user/delete/{id}');
Route::post('/user/create', 'UserController@Create')->name('/user/create');
Route::post('/user/update/{id}', 'UserController@Update')->name('/user/update/{id}');
