@extends('layouts.app')
@section('content')

<div id="content-header">
	<div id="breadcrumb"> <a href="index.html" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a></div>
</div>
<div class="container-fluid">
	<div class="quick-actions_homepage">
		<ul class="quick-actions">
			<li> <a href="{{ route('/authors') }}"> <i class="icon-people"></i>  Авторов <br> {{$authors}} </a> </li>
			<li> <a href="{{ route('/books/{user_id}', ['user_id' => $user->id]) }}"> <i class="icon-book"></i> Ваших книг <br> {{$user->countBooks()}} </a> </li>
			<li> <a href="{{ route('/all-books') }}"> <i class="icon-book"></i> Всего книг <br> {{$books}} </a> </li>
		</ul>
	</div>
</div>
@endsection
