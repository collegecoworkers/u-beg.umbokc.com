<!DOCTYPE html>
<html lang="en">
<head>
	<title>{{ config('app.name', 'Laravel') }}</title>
	<meta charset="UTF-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<link rel="stylesheet" href="http://cdn.uwebu.ru/u-emmet-attr/src/ea.css?v=1.2">
	<link rel="stylesheet" href="{{ asset('assets/css/bootstrap.min.css') }}" />
	<link rel="stylesheet" href="{{ asset('assets/css/bootstrap-responsive.min.css') }}" />
	<link rel="stylesheet" href="{{ asset('assets/css/fullcalendar.css') }}" />
	<link rel="stylesheet" href="{{ asset('assets/css/maruti-style.css') }}" />
	<link rel="stylesheet" href="{{ asset('assets/css/maruti-media.css') }}" class="skin-color" />
	<style>
	#sidebar > ul > li.active {
		background-color: #41bedd;
	}
	</style>
</head>
<body ea>
	<!--Header-part-->
	<div id="header">
		<h1><a href="/">{{ config('app.name', 'Laravel') }}</a></h1>
	</div>
	<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">{{ csrf_field() }}</form>
	<div id="user-nav" class="navbar navbar-inverse">
		<ul class="nav">
			<li class=""><a title="" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();"><i class="icon icon-share-alt"></i> <span class="text">Выход</span></a></li>
		</ul>
	</div>
	<!--close-Header-part--> 

	<div id="sidebar">
		<ul>
			<li class="{{ url()->current() == route('/') ? 'active' : '' }}"><a href="/"><i class="icon icon-home"></i> <span>Главная</span></a> </li>
			<li class="{{ url()->current() == route('/books/{user_id}', ['user_id'=>auth()->user()->id]) ? 'active' : '' }}"><a href="{{ route('/books/{user_id}', ['user_id'=>auth()->user()->id]) }}"><i class="icon icon-book"></i> <span>Мои книги</span></a> </li>
			<li class="{{ url()->current() == route('/all-books') ? 'active' : '' }}"><a href="{{ route('/all-books') }}"><i class="icon icon-folder-open"></i> <span>Все книги</span></a> </li>
			<li class="{{ url()->current() == route('/authors') ? 'active' : '' }}"><a href="{{ route('/authors') }}"><i class="icon icon-user"></i> <span>Авторы</span></a> </li>
			@if (auth()->user()->role == 'admin')
				<li class="{{ url()->current() == route('/users') ? 'active' : '' }}"><a href="{{ route('/users') }}"><i class="icon icon-user"></i> <span>Пользователи</span></a> </li>
			@endif
		</ul>
	</div>
	<div id="content">
		@yield('content')
	</div>
	<div class="row-fluid">
		<div id="footer" class="span12"> 2012 &copy; {{ config('app.name', 'Laravel') }}.</div>
	</div>
	<script src="{{ asset('assets/js/excanvas.min.js') }}"></script> 
	<script src="{{ asset('assets/js/jquery.min.js') }}"></script> 
	<script src="{{ asset('assets/js/jquery.ui.custom.js') }}"></script> 
	<script src="{{ asset('assets/js/bootstrap.min.js') }}"></script> 
	<script src="{{ asset('assets/js/jquery.flot.min.js') }}"></script> 
	<script src="{{ asset('assets/js/jquery.flot.resize.min.js') }}"></script> 
	<script src="{{ asset('assets/js/jquery.peity.min.js') }}"></script> 
	<script src="{{ asset('assets/js/fullcalendar.min.js') }}"></script> 
	<script src="{{ asset('assets/js/maruti.js') }}"></script> 
	<script src="{{ asset('assets/js/maruti.dashboard.js') }}"></script> 
	<script src="{{ asset('assets/js/maruti.chat.js') }}"></script> 

</body>
</html>
