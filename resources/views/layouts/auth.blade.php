<!DOCTYPE html>
<html lang="en">
<head>
	<title>{{ config('app.name', 'Laravel') }}</title><meta charset="UTF-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<link rel="stylesheet" href="{{ asset('assets/css/bootstrap.min.css') }}" />
	<link rel="stylesheet" href="{{ asset('assets/css/bootstrap-responsive.min.css') }}" />
	<link rel="stylesheet" href="{{ asset('assets/css/maruti-login.css') }}" />
</head>
<body>
	<div id="loginbox">
		@yield('content')
	</div>
	<script src="{{ asset('assets/js/jquery.min.js') }}"></script>  
	<script src="{{ asset('assets/js/maruti.login.js') }}"></script> 
</body>

</html>
