@extends('../layouts.auth')
@section('content')
<form method="post" class="form-vertical" action="{{ route('login') }}">
	{{ csrf_field() }}
	<div class="control-group normal_text">
		<h3>{{ config('app.name', 'Laravel') }}<br>Вход</h3>
	</div>
	<div class="control-group">
		<div class="controls">
			<div class="main_input_box">
				<span class="add-on"><i class="icon-user"></i></span><input type="email" name="email" placeholder="Email" />
				@if ($errors->has('email'))<span class="help-block"><strong c#f>{{ $errors->first('email') }}</strong></span> @endif
			</div>
		</div>
	</div>
	<div class="control-group">
		<div class="controls">
			<div class="main_input_box">
				<span class="add-on"><i class="icon-lock"></i></span><input type="password" name="password" placeholder="Пароль" />
				@if ($errors->has('password'))<span class="help-block"><strong c#f>{{ $errors->first('password') }}</strong></span> @endif
			</div>
		</div>
	</div>
	<div class="form-actions">
		<span class="pull-left"><a href="{{ route('register') }}" class="btn btn-inverse">Зарегистрироваться</a></span>
		<span class="pull-right"><input type="submit" class="btn btn-success" value="Войти" /></span>
	</div>
</form>
@endsection
