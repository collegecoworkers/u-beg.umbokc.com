@php
	use App\User;
@endphp
@extends('layouts.app')
@section('content')
<div id="content-header">
	<div id="breadcrumb">
		<a href="/" title="Go to Home" class="tip-bottom">
			<i class="icon-home"></i> Главная</a>
		<a href="{{ url()->current() }}" class="current">Список книг</a>
	</div>
	<h1>Список книг пользователя: {{ $user->full_name }}</h1>
</div>
<div class="container-fluid">
	<div class="row-fluid">
		<div class="span12">
			@if ($user->id == User::curr()->id)
			<div>
				<a href="{{ route('/book/add') }}" class="btn btn-primary">Добавить</a>
			</div>
			@endif
			<div class="widget-box">
				<div class="widget-title">
					<span class="icon"><i class="icon-th"></i></span> 
					<h5>Список книг</h5>
				</div>
				<div class="widget-content nopadding">
					<table class="table table-bordered data-table">
						<thead>
							<tr>
								<th>#</th>
								<th>Название</th>
								<th>Описание</th>
								<th>Действия</th>
							</tr>
						</thead>
						<tbody>
							@foreach ($books as $item)
							<tr class="gradeX">
								<td ta:c>{{$item->id}}</td>
								<td ta:c>{{$item->title}}</td>
								<td ta:c>{{$item->desc}}</td>
								<td ta:c>
									<a href="{{ route('/book/view/{id}', ['id'=>$item->id]) }}">
										<i class="icon icon-eye-open"></i>
									</a>
									@if ($user->id == User::curr()->id)
										<a href="{{ route('/book/edit/{id}', ['id'=>$item->id]) }}">
											<i class="icon icon-pencil"></i>
										</a>
										<a href="{{ route('/book/delete/{id}', ['id'=>$item->id]) }}" onclick="return confirm('Вы уверенны?')">
											<i class="icon icon-trash"></i>
										</a>
									@endif
								</td>
							</tr>
							@endforeach
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>

</div>
@endsection
