@extends('layouts.app')
@section('content')
<div id="content-header">
	<div id="breadcrumb">
		<a href="/" title="Go to Home" class="tip-bottom">
			<i class="icon-home"></i> Главная</a>
			<a href="{{ url()->current() }}" class="current">Список пользователи</a>
		</div>
		<h1>Список пользователи</h1>
	</div>
	<div class="container-fluid">
		<div class="row-fluid">
			<div class="span12">
				<div>
					<a href="{{ route('/user/add') }}" class="btn btn-primary">Добавить</a>
				</div>
				<div class="widget-box">
					<div class="widget-title">
						<span class="icon"><i class="icon-th"></i></span> 
						<h5>Список</h5>
					</div>
					<div class="widget-content nopadding">
						<table class="table table-bordered data-table">
							<thead>
								<tr>
									<th>#</th>
									<th>Имя</th>
									<th>Логин</th>
									<th>Email</th>
									<th>Статус</th>
									<th>Действия</th>
								</tr>
							</thead>
							<tbody>
								@foreach ($users as $item)
								<tr class="gradeX">
									<td ta:c>{{$item->id}}</td>
									<td ta:c>{{$item->full_name}}</td>
									<td ta:c>{{$item->name}}</td>
									<td ta:c>{{$item->email}}</td>
									<td ta:c>{{$item->getRole()}}</td>
									<td ta:c>
										<a href="{{ route('/user/edit/{id}', ['id'=>$item->id]) }}">
											<i class="icon icon-pencil"></i>
										</a>
										<a href="{{ route('/user/delete/{id}', ['id'=>$item->id]) }}" onclick="return confirm('Вы уверенны?')">
											<i class="icon icon-trash"></i>
										</a>
									</td>
								</tr>
								@endforeach
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>

</div>
@endsection
