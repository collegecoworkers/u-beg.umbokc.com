@extends('layouts.app')
@section('content')
<div id="content-header">
	<div id="breadcrumb">
		<a href="/" title="Go to Home" class="tip-bottom">
			<i class="icon-home"></i> Главная</a>
		<a href="{{ url()->current() }}" class="current">Список авторов</a>
	</div>
	<h1>Список авторов</h1>
</div>
<div class="container-fluid">
	<div class="row-fluid">
		<div class="span12">
			<div class="widget-box">
				<div class="widget-title">
					<span class="icon"><i class="icon-th"></i></span> 
					<h5>Список авторов</h5>
				</div>
				<div class="widget-content nopadding">
					<table class="table table-bordered data-table">
						<thead>
							<tr>
								<th>#</th>
								<th>Имя</th>
								<th>Кол-во книг</th>
								<th>Действия</th>
							</tr>
						</thead>
						<tbody>
							@foreach ($users as $item)
							@if ($item->countBooks() <= 0)
								@continue
							@endif
							<tr class="gradeX">
								<td ta:c>{{$item->id}}</td>
								<td ta:c>{{$item->full_name}}</td>
								<td ta:c>{{$item->countBooks()}}</td>
								<td ta:c>
									<a href="{{ route('/books/{user_id}', ['user_id'=>$item->id]) }}">
										<i class="icon icon-eye-open"></i>
									</a>
								</td>
							</tr>
							@endforeach
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>

</div>
@endsection
