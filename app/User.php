<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
	use Notifiable;

	protected $fillable = [
		'name', 'email', 'password',
	];

	protected $hidden = [
		'password', 'remember_token'
	];

	public function getRole() {
		return self::getRoleOf($this->role);
	}

	public static function getRoles(){
		return [
			'user' => 'Пользователь',
			'admin' => 'Админ',
		];
	}

	public static function getRoleOf($r){
		$roles = self::getRoles();
		if(array_key_exists($r, $roles)) 
			return $roles[$r];
		return $roles[0];
	}

	public static function isAdmin() {
		return self::curr()->role == 'admin';
	}

	public static function isUser() {
		return self::curr()->role == 'user';
	}

	public static function curr() {
		return auth()->user();
	}

	public static function curRole() {
		return self::curr()->role;
	}
	private $count_books = null;
	public function countBooks() {
		if($this->count_books === null){
			$this->count_books = Book::getsBy('user_id', $this->id)->count();
		}
		return $this->count_books;
	}

	public static function getById($val){
		return self::getBy('id', $val);
	}
	public static function getBy($col, $val = null){
		if(is_array($col)) return self::queryBy($col)->first();
		else return self::queryBy([$col => $val])->first();
	}
	public static function getsBy($col, $val = null){
		if(is_array($col)) return self::queryBy($col)->get();
		else return self::queryBy([$col => $val])->get();
	}
	public static function queryBy($arr){
		return self::where($arr);
	}
}
