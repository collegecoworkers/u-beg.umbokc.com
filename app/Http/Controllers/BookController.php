<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
// use Illuminate\Http\Response;
use PDF;
use App\{
	F,
	User,
	Book
};

class BookController extends Controller
{

	public function __construct(){
		$this->middleware('auth');
	}

	public function View($id) {
		$this->the_before();
		$book = Book::getById($id);
		$name = $book->title.'.pdf';
		if($book->generated == 0){
			$html = '<title>'.$name.'</title>'.$book->data;
			$book->generated = 1;
			$book->save();
			return PDF::loadHTML($html)
						->setPaper('a4', 'portrait')
						->setWarnings(false)
						->save($book->getPath())
						->stream($name);
		} else {
			$path = $book->getPath();
			return response()->file($path, [
				'Content-Type' => 'application/pdf',
				'Content-Disposition' => 'inline; filename="'.$name.'"'
			]);
		}
	}
	public function Add() {
		$this->the_before();
		return view('book.add')->with([
			'cur_role' => $this->cur_role,
			'user' => User::curr(),
		]);
	}
	public function Edit($id) {
		$this->the_before();
		$model = Book::getBy('id', $id);
		return view('book.edit')->with([
			'cur_role' => $this->cur_role,
			'model' => $model,
		]);
	}
	public function Delete($id) {
		$this->the_before();
		Book::where('id', $id)->delete();
		return redirect()->to('/books/'.User::curr()->id);
	}
	public function Create(Request $request) {
		$this->the_before();
		$model = new Book();

		$model->title = request()->title;
		$model->desc = request()->desc;
		$model->data = request()->data;
		$model->user_id = User::curr()->id;
		$model->generated = 0;
		$model->path = '';

		$model->save();
		return redirect()->to('/books/'.User::curr()->id);
	}
	public function Update($id, Request $request) {
		$this->the_before();
		$model = Book::getBy('id', $id);

		$model->title = request()->title;
		$model->desc = request()->desc;
		$model->data = request()->data;
		$model->generated = 0;
		$model->path = '';

		$model->save();
		return redirect()->to('/books/'.User::curr()->id);
	}

	public function the_before() {
		$this->cur_role = User::curRole();
	}
}
