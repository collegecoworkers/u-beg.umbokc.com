<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\{
	User,
	Book
};

class SiteController extends Controller
{

	public function __construct(){
		$this->middleware('auth');
	}

	public function Index() {
		$this->the_before();
		$authors = 0;
		$users = User::all();
		foreach ($users as $item) {
			if($item->countBooks() > 0)
				$authors++;
		}

		return view('index')->with([
			'cur_role' => $this->cur_role,
			'authors' => $authors,
			'user' => User::curr(),
			'books' => Book::count(),
		]);
	}

	public function AllBooks() {
		$this->the_before();
		$books = Book::all();
		return view('booksall')->with([
			'cur_role' => $this->cur_role,
			'books' => $books,
		]);
	}

	public function Books($id) {
		$this->the_before();
		$user = User::getById($id);
		$books = Book::getsBy('user_id', $user->id);
		return view('books')->with([
			'cur_role' => $this->cur_role,
			'books' => $books,
			'user' => $user,
		]);
	}

	public function Authors() {
		$this->the_before();
		$users = User::all();
		return view('authors')->with([
			'cur_role' => $this->cur_role,
			'users' => $users,
		]);
	}

	public function Users() {
		$this->the_before();
		$users = User::all();
		return view('users')->with([
			'cur_role' => $this->cur_role,
			'users' => $users,
		]);
	}

	public function the_before() {
		$this->cur_role = User::curRole();
	}
}
