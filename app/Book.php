<?php

namespace App;

class Book extends MyModel{
	public function getPath() {
    if($this->path == ''){
      $this->path = public_path()."/books/{$this->user_id}_{$this->id}.pdf";
      $this->save();
    }
		return $this->path;
	}
}
